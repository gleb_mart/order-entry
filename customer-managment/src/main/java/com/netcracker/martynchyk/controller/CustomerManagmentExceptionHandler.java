package com.netcracker.martynchyk.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomerManagmentExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    protected ResponseEntity<Object> handleNullPointerException(NullPointerException exc, WebRequest request) {
        String bodyOfResponce="There is no such content";
        return handleExceptionInternal(exc,bodyOfResponce,
                new HttpHeaders(), HttpStatus.NOT_FOUND,request);
    }

}
