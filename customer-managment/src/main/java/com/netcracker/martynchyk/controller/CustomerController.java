package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.CustomerDto;
import com.netcracker.martynchyk.entity.Customer;
import com.netcracker.martynchyk.services.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/customer-management/customers")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ApiOperation(value = "Return customer by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto get(
            @ApiParam(value = "Id of customer to lookup for", required = true)
            @PathVariable("id") Long id) {
        return customerService.find(id).convertToDto();
    }

    @ApiOperation(value = "Return all customers")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CustomerDto> getAll() {
        List<Customer> customers = customerService.findAll();
        return customers.stream()
                .map(customer -> customer.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create customer")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerDto createCustomer(@RequestBody @Valid CustomerDto customerDto) {
        Customer customer = Customer.convertToEntity(customerDto);
        Customer customerCreated = customerService.create(customer);
        return customerCreated.convertToDto();
    }

    @ApiOperation(value = "Update customer by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto updateCustomer(
            @PathVariable("id") Long id,
            @RequestBody @Valid CustomerDto customerDto) {
        Customer customer = customerService.find(id);
        customer.setName(customerDto.getName());
        customer.setAge(customerDto.getAge());
        customer.setEmail(customerDto.getEmail());
        return customerService.update(customer).convertToDto();
    }

    @ApiOperation(value = "Delete customer by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable("id") Long id) {
        customerService.delete(id);
    }
}
