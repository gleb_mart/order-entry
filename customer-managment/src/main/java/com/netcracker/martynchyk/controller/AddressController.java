package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.AddressDto;
import com.netcracker.martynchyk.entity.Address;
import com.netcracker.martynchyk.entity.Customer;
import com.netcracker.martynchyk.services.AddressService;
import com.netcracker.martynchyk.services.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/customer-management/addresses")
public class AddressController {

    private final AddressService addressService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @ApiOperation(value = "Return address by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AddressDto get(
            @ApiParam(value = "Id of address to lookup for", required = true)
            @PathVariable("id") Long id) {
        return addressService.find(id).convertToDto();
    }

    @ApiOperation(value = "Return all addresses")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<AddressDto> getAll() {
        List<Address> addresses = addressService.findAll();
        return addresses.stream()
                .map(address -> address.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create address")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AddressDto createAddress(@RequestBody @Valid AddressDto addressDto) {
        Address address = Address.convertToEntity(addressDto);
        Customer findCustomer = customerService.find(addressDto.getCustomer());
        address.setCustomer(findCustomer);
        Address addressCreated = addressService.create(address);
        return addressCreated.convertToDto();
    }

    @ApiOperation(value = "Update address by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AddressDto updateAddress(
            @PathVariable("id") Long id,
            @RequestBody @Valid AddressDto addressDto) {
        Address address = addressService.find(id);
        address.setCity(addressDto.getCity());
        address.setStreet(addressDto.getStreet());
        address.setHouseNumber(addressDto.getHouseNumber());
        if(addressDto.getCustomer()!=address.getCustomer().getId()){
            Customer findCustomer =customerService.find(addressDto.getCustomer());
            address.setCustomer(findCustomer);
        }
        return addressService.update(address).convertToDto();
    }

    @ApiOperation(value = "Delete address by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAddress(@PathVariable("id") Long id) {
        addressService.delete(id);
    }
}
