package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.AddressDao;
import com.netcracker.martynchyk.dao.implementation.DefaultAddressDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk.config")
public class DefaultAddressServiceConfig {
    @Bean
    public AddressDao addressDao() {
        return new DefaultAddressDao();
    }
}
