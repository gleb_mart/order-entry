package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.CustomerDao;
import com.netcracker.martynchyk.dao.implementation.DefaultCustomerDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk.config")
public class DefaultCustomerServiceConfig {
    @Bean
    public CustomerDao customerDao() {
        return new DefaultCustomerDao();
    }
}
