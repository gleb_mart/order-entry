package com.netcracker.martynchyk.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PostgreSQLDatabaseManager implements DatabaseManager {

    private static EntityManagerFactory ENTITY_MANAGER_FACTORY  = Persistence
            .createEntityManagerFactory("customersUnit");

    private EntityManager entityManager;

    private PostgreSQLDatabaseManager() {
        entityManager =  ENTITY_MANAGER_FACTORY.createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }


    private static class Holder {
        private static final DatabaseManager INSTANCE = new PostgreSQLDatabaseManager();
    }

    public static DatabaseManager getInstance() {
        return Holder.INSTANCE;
    }


}
