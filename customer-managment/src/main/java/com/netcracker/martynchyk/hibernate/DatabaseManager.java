package com.netcracker.martynchyk.hibernate;

import javax.persistence.EntityManager;

public interface DatabaseManager {
   EntityManager getEntityManager();
}
