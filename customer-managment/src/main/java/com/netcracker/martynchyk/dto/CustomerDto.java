package com.netcracker.martynchyk.dto;

import com.netcracker.martynchyk.entity.Address;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CustomerDto {

    @NotBlank
    private String name;
    @NotNull
    @Min(12)
    private byte age;
    @Email
    private String email;
    private AddressDto address;

    public CustomerDto(String name,byte age,String email, AddressDto address) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public byte getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public AddressDto getAddress() {
        return address;
    }
}
