package com.netcracker.martynchyk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AddressDto {
    @NotBlank
    private String city;
    @NotBlank
    private String street;
    @NotBlank
    private String houseNumber;
    @NotNull
    @Min(1)
    private Long customer;

    public AddressDto(String city, String street, String houseNumber, Long customer) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.customer = customer;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    @JsonIgnore
    public Long getCustomer() {
        return customer;
    }
}
