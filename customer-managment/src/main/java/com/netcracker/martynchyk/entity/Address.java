package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.AddressDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Address extends BaseEntity {
    @Basic(optional = false)
    private String city;
    @Basic(optional = false)
    private String street;
    @Basic(optional = false)
    private String houseNumber;
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Customer customer;

    public Address() {
    }

    public Address(String city, String street, String houseNumber) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public Address(String city, String street, String houseNumber, Customer customer) {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.customer = customer;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public static Address convertToEntity(AddressDto addressDto) {
        Customer customer=new Customer();
        customer.setId(addressDto.getCustomer());
        return new Address(addressDto.getCity(), addressDto.getStreet(),
                addressDto.getHouseNumber());
    }

    public AddressDto convertToDto() {
        return new AddressDto(city, street, houseNumber, customer.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(city, address.city) &&
                Objects.equals(street, address.street) &&
                Objects.equals(houseNumber, address.houseNumber) &&
                Objects.equals(customer, address.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, street, houseNumber, customer);
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                '}';
    }
}
