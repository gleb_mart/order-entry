package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.CustomerDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Customer extends BaseEntity {

    @Basic(optional = false)
    private String name;

    @Basic(optional = false)
    private byte age;

    @Basic(optional = false)
    private String email;

    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,optional = false)
    private Address address;

    public Customer() {
    }

    public Customer(String name, byte age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public static Customer convertToEntity(CustomerDto customerDto) {
        return new Customer(customerDto.getName(), customerDto.getAge(),
                customerDto.getEmail());
    }

    public CustomerDto convertToDto() {
        if (address==null){
            return new CustomerDto(name, age, email, null);
        }
        else {
            return new CustomerDto(name, age, email, address.convertToDto());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return age == customer.age &&
                Objects.equals(name, customer.name) &&
                Objects.equals(email, customer.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, email);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }
}
