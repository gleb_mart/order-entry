package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.entity.Customer;
import com.netcracker.martynchyk.dao.CustomerDao;
import com.netcracker.martynchyk.hibernate.PostgreSQLDatabaseManager;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@Repository
public class DefaultCustomerDao implements CustomerDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    public DefaultCustomerDao() {
    }

    @Override
    public Customer create(Customer customer) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(customer);
        tx.commit();
        return customer;
    }

    @Override
    public Customer find(long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Customer foundCustomer;
        tx.begin();
        foundCustomer = entityManager.find(Customer.class, id);
        tx.commit();
        return foundCustomer;
    }

    @Override
    public List<Customer> findAll() {
        EntityTransaction tx = entityManager.getTransaction();
        List<Customer> customers;
        tx.begin();
        customers = entityManager.createQuery("SELECT customer FROM Customer customer", Customer.class).getResultList();
        tx.commit();
        return customers;
    }

    @Override
    public Customer update(Customer customer) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(customer);
        tx.commit();
        return customer;
    }

    @Override
    public void delete(long id) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        Customer customer = entityManager.getReference(Customer.class, id);
        entityManager.remove(customer);
        tx.commit();
    }
}
