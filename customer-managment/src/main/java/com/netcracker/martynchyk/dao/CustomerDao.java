package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Customer;

import java.util.List;

public interface CustomerDao {

    Customer create(Customer customer);

    Customer find(long id);

    List<Customer> findAll();

    Customer update(Customer customer);

    void delete(long id);
}
