package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Address;

import java.util.List;

public interface AddressDao {

    Address create(Address address);

    Address find(long id);

    List<Address> findAll();

    Address update(Address address);

    void delete(long id);
}
