package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.entity.Address;
import com.netcracker.martynchyk.dao.AddressDao;
import com.netcracker.martynchyk.hibernate.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class DefaultAddressDao implements AddressDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    public DefaultAddressDao() {
    }

    @Override
    public Address create(Address address) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(address);
        tx.commit();
        return address;
    }

    @Override
    public Address find(long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Address foundAddress;
        tx.begin();
        foundAddress = entityManager.find(Address.class, id);
        tx.commit();
        return foundAddress;
    }

    @Override
    public List<Address> findAll() {
        EntityTransaction tx = entityManager.getTransaction();
        List<Address> addresses;
        tx.begin();
        addresses = entityManager.createQuery("SELECT address FROM Address address", Address.class).getResultList();
        tx.commit();
        return addresses;
    }

    @Override
    public Address update(Address address) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(address);
        tx.commit();
        return address;
    }

    @Override
    public void delete(long id) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        Address address = entityManager.getReference(Address.class, id);
        entityManager.remove(address);
        tx.commit();
    }
}
