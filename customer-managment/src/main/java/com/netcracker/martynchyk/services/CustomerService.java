package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.entity.Customer;

import java.util.List;


public interface CustomerService {

    Customer create(Customer customer);

    Customer find(long id);

    List<Customer> findAll();

    Customer update(Customer customer);

    void delete(long id);
}
