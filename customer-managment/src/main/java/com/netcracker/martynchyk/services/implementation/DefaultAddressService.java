package com.netcracker.martynchyk.services.implementation;

import com.netcracker.martynchyk.entity.Address;
import com.netcracker.martynchyk.dao.AddressDao;
import com.netcracker.martynchyk.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAddressService implements AddressService {

    private final AddressDao addressDao;

    @Autowired
    public DefaultAddressService(AddressDao addressDao) {
        this.addressDao = addressDao;
    }

    @Override
    public Address create(Address address) {
        addressDao.create(address);
        return address;
    }

    @Override
    public Address find(long id) {
        return addressDao.find(id);
    }

    @Override
    public List<Address> findAll() {
        return addressDao.findAll();
    }

    @Override
    public Address update(Address address) {
        addressDao.update(address);
        return address;
    }

    @Override
    public void delete(long id) {
        addressDao.delete(id);
    }
}
