package com.netcracker.martynchyk.services.implementation;

import com.netcracker.martynchyk.entity.Customer;
import com.netcracker.martynchyk.dao.CustomerDao;
import com.netcracker.martynchyk.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultCustomerService implements CustomerService {

    private final CustomerDao customerDao;

    @Autowired
    public DefaultCustomerService(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    public Customer create(Customer customer) {
        customerDao.create(customer);
        return customer;
    }

    @Override
    public Customer find(long id) {
        return customerDao.find(id);
    }

    @Override
    public List<Customer> findAll() {
        return customerDao.findAll();
    }

    @Override
    public Customer update(Customer customer) {
        customerDao.update(customer);
        return customer;
    }

    @Override
    public void delete(long id) {
        customerDao.delete(id);
    }
}
