package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Customer;
import com.netcracker.martynchyk.dao.implementation.DefaultCustomerDao;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class DefaultCustomerDaoTest {

    private DefaultCustomerDao customerDao;
    private Customer testCustomer;
    private Customer testCustomer1;
    private Customer createdCustomer;
    private Customer createdCustomer1;

    @Before
    public void setUp() {
        customerDao = new DefaultCustomerDao();
        testCustomer = new Customer("Vasya", (byte) 15, "mail");
        testCustomer1 = new Customer("Kolya", (byte) 12, "yandex");
    }

    @Test
    public void testCreateCustomer() {
        Customer createdCustomer = customerDao.create(testCustomer);
        assertNotNull(createdCustomer.getId());
    }

    @Test
    public void testFindAllCustomer() {
        Customer createdCustomer = customerDao.create(testCustomer);
        Customer createdCustomer1 = customerDao.create(testCustomer1);

        List<Customer> expectedCustomers = new ArrayList<>();
        expectedCustomers.add(createdCustomer);
        expectedCustomers.add(createdCustomer1);

        List<Customer> actualCustomers = customerDao.findAll();

        assertNotNull(actualCustomers);
        assertEquals(expectedCustomers, actualCustomers);
    }

    @Test
    public void testFindCustomer() {
        Customer createdCustomer = customerDao.create(testCustomer);

        Customer foundCustomer = customerDao.find(testCustomer.getId());

        assertNotNull(foundCustomer.getId());
        assertEquals(createdCustomer.getId(), foundCustomer.getId());
    }

    @Test
    public void testUpdateSingleUser() {
        Customer createdCustomer = customerDao.create(testCustomer);
        createdCustomer.setName("Updated name");

        Customer updatedCustomer = customerDao.update(createdCustomer);

        assertNotNull(updatedCustomer.getId());
        assertEquals(createdCustomer.getId(), updatedCustomer.getId());
        assertEquals(updatedCustomer.getName(), "Updated name");
    }

    @Test
    public void testDeleteSingleUser() {
        Customer createdCustomer = customerDao.create(testCustomer);
        Long id = createdCustomer.getId();

        customerDao.delete(id);

        Customer deletedCustomer = customerDao.find(id);

        assertNull(deletedCustomer);
    }

}
