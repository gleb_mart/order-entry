package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Address;
import com.netcracker.martynchyk.entity.Customer;
import com.netcracker.martynchyk.dao.implementation.DefaultAddressDao;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DefaultAddressDaoTest {

    private DefaultAddressDao addressDao;
    private Customer testCustomer;
    private Address testAddress;
    private Address testAddress1;

    @Before
    public void setUp() {
        addressDao = new DefaultAddressDao();
        testCustomer = new Customer("Vasya", (byte) 15, "mail");
        //testCustomer.setId(1L);

        testAddress = new Address("Minsk", "sovetskaya",
                "154", testCustomer);

        testAddress1 = new Address("Moscow", "leninsakaya",
                "228", testCustomer);
    }

    @Test
    public void testCreateCustomer() {
        Address expectedAddress = addressDao.create(testAddress);
        assertNotNull(expectedAddress.getId());
    }

    @Test
    public void testFindAllCustomer() {
        Address expectedAddress = addressDao.create(testAddress);
        Address expectedAddress1 = addressDao.create(testAddress1);

        List<Address> expectedAddresses = new ArrayList<>();
        expectedAddresses.add(expectedAddress);
        expectedAddresses.add(expectedAddress1);

        List<Address> actualAddresses = addressDao.findAll();

        assertNotNull(actualAddresses);
        assertEquals(expectedAddresses, actualAddresses);
    }

    @Test
    public void testFindCustomer() {
        Address expectedAddress = addressDao.create(testAddress);

        Address foundCustomer = addressDao.find(testAddress.getId());

        assertNotNull(foundCustomer.getId());
        assertEquals(expectedAddress.getId(), foundCustomer.getId());
    }

    @Test
    public void testUpdateSingleUser() {
        Address expectedAddress = addressDao.create(testAddress);
        expectedAddress.setHouseNumber("505");

        Address updatedCustomer = addressDao.update(expectedAddress);

        assertNotNull(updatedCustomer.getId());
        assertEquals(expectedAddress.getId(), updatedCustomer.getId());
        assertEquals(updatedCustomer.getHouseNumber(), "505");
    }

    @Test
    public void testDeleteSingleUser() {
        Address expectedAddress = addressDao.create(testAddress);
        Long id = expectedAddress.getId();

        addressDao.delete(id);

        Address deletedCustomer = addressDao.find(id);

        assertNull(deletedCustomer);
    }


}
