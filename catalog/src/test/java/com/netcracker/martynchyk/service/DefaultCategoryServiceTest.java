package com.netcracker.martynchyk.service;

import com.netcracker.martynchyk.config.CategoryConfig;
import com.netcracker.martynchyk.config.DefaultCategoryServiceConfig;
import com.netcracker.martynchyk.config.OfferConfig;
import com.netcracker.martynchyk.config.TagConfig;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.services.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


@ContextConfiguration(classes = {DefaultCategoryServiceConfig.class})
public class DefaultCategoryServiceTest {

    @Autowired
    private CategoryService categoryDao;
    private ApplicationContext offerContext;
    private ApplicationContext categoryContext;
    private ApplicationContext tagContext;
    private Category testCategory;
    private Category testCategory1;

    @Before
    public void setUp() {

        offerContext = new AnnotationConfigApplicationContext(OfferConfig.class);
        categoryContext = new AnnotationConfigApplicationContext(CategoryConfig.class);
        tagContext = new AnnotationConfigApplicationContext(TagConfig.class);

        Offer testOffer= offerContext.getBean("offerA",Offer.class);
        Offer testOffer1= offerContext.getBean("offerB",Offer.class);

        Set<Offer> offers = new HashSet<>();
        offers.add(testOffer);
        offers.add(testOffer1);

        testCategory = categoryContext.getBean("categoryA",Category.class);
        testCategory1 = categoryContext.getBean("categoryB",Category.class);

        testCategory.setCategory("shooter");
        testCategory.setOffer(offers);
        testCategory1.setCategory("strategy");
        testCategory1.setOffer(offers);

        Tag testTag =tagContext.getBean("tagA",Tag.class);
        Tag testTag1 =tagContext.getBean("tagB",Tag.class);

        testOffer.setName("COD");
        testOffer.setPrice(new BigDecimal(60));
        testOffer.setCategory(testCategory);
        testOffer.setTags(null);

        testOffer1.setName("COD");
        testOffer1.setPrice(new BigDecimal(60));
        testOffer1.setCategory(testCategory);
        testOffer1.setTags(null);

        testTag.setName("hot");
        testTag.setOffers(offers);
        testTag1.setName("new");
        testTag1.setOffers(offers);
    }

    @Test
    public void testCreateCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        assertNotNull(expectedCategory.getId());
    }

    @Test
    public void testFindAllCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        Category expectedCategory1 = categoryDao.create(testCategory1);

        List<Category> expectedCategories = new ArrayList<>();
        expectedCategories.add(expectedCategory);
        expectedCategories.add(expectedCategory1);

        List<Category> actualCategories = categoryDao.findAll();

        assertNotNull(actualCategories);
        assertEquals(expectedCategories, actualCategories);
    }

    @Test
    public void testFindCategory() {
        Category expectedCategory = categoryDao.create(testCategory);

        Category actualCategory = categoryDao.find(testCategory.getId());

        assertNotNull(actualCategory.getId());
        assertEquals(expectedCategory.getId(), actualCategory.getId());
    }

    @Test
    public void testUpdateSingleCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        expectedCategory.setCategory("updated category");

        Category updatedCategory = categoryDao.update(expectedCategory);

        assertNotNull(updatedCategory.getId());
        assertEquals(expectedCategory.getId(), updatedCategory.getId());
        assertEquals("updated category", updatedCategory.getCategory());
    }

    @Test
    public void testDeleteSingleCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        Long id = expectedCategory.getId();

        categoryDao.delete(id);

        Category deletedCategory = categoryDao.find(id);

        assertNull(deletedCategory);
    }

    @Test
    public void testCreateSeveralCategories() {
        List<Category> testCategories = new ArrayList<>();
        testCategories.add(testCategory);
        testCategories.add(testCategory1);
        List<Category> expectedCategories = categoryDao.createSeveralCategories(testCategories);
        for (Category expectedCategory : expectedCategories) {
            assertNotNull(expectedCategory.getId());
        }
    }

}
