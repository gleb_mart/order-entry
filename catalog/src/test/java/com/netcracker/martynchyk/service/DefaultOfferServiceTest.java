package com.netcracker.martynchyk.service;


import com.netcracker.martynchyk.config.CategoryConfig;
import com.netcracker.martynchyk.config.DefaultOfferServiceConfig;
import com.netcracker.martynchyk.config.OfferConfig;
import com.netcracker.martynchyk.config.TagConfig;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.services.OfferService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@ContextConfiguration(classes = {DefaultOfferServiceConfig.class})
public class DefaultOfferServiceTest {

    @Autowired
    private OfferService offerService;
    private ApplicationContext offerContext;
    private Offer testOffer;
    private Offer testOffer1;
    private Set<Offer> offers;

    @Before
    public void setUp() {
        offerContext = new AnnotationConfigApplicationContext(OfferConfig.class);

        testOffer = offerContext.getBean("offerA", Offer.class);
        testOffer1 = offerContext.getBean("offerB", Offer.class);

        offers = new HashSet<>();
        offers.add(testOffer);
        offers.add(testOffer1);

        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("hot", offers));
        tags.add(new Tag("new", offers));

        testOffer.setName("COD");
        testOffer.setPrice(new BigDecimal(60));
        testOffer.setCategory(new Category("shooter", offers));
        testOffer.setTags(tags);

        testOffer1.setName("Total War");
        testOffer1.setPrice(new BigDecimal(30));
        testOffer1.setCategory(new Category("strategy", offers));
        testOffer1.setTags(tags);
    }

    @Test
    public void testCreateOffer() {
        Offer expectedOffer = offerService.create(testOffer);
        assertNotNull(expectedOffer.getId());
    }

    @Test
    public void testFindAllOffer() {
        Offer expectedOffer = offerService.create(testOffer);
        Offer expectedOffer1 = offerService.create(testOffer1);

        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedOffer);
        expectedOffers.add(expectedOffer1);

        List<Offer> actualOffers = offerService.findAll();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }

    @Test
    public void testFindAllByPrice() {
        Offer expectedOffer = offerService.create(testOffer);
        Offer expectedOffer1 = offerService.create(testOffer1);

        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedOffer1);
        expectedOffers.add(expectedOffer);

        List<Offer> actualOffers = offerService.findAllByPrice();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }

    @Test
    public void testFindAllByCategory() {
        Offer expectedOffer = offerService.create(testOffer);
        Offer expectedOffer1 = offerService.create(testOffer1);

        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedOffer1);
        expectedOffers.add(expectedOffer);

        List<Offer> actualOffers = offerService.findAllByCategory();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }


    @Test
    public void testFindOffer() {
        Offer expectedOffer = offerService.create(testOffer);

        Offer actualOffer = offerService.find(testOffer.getId());

        assertNotNull(actualOffer.getId());
        assertEquals(expectedOffer.getId(), actualOffer.getId());
    }

    @Test
    public void testUpdateSingleOffer() {
        Offer expectedOffer = offerService.create(testOffer);
        expectedOffer.setName("upd category");

        Offer updatedOffer = offerService.update(expectedOffer);

        assertNotNull(updatedOffer.getId());
        assertEquals(expectedOffer.getId(), updatedOffer.getId());
        assertEquals("upd category", updatedOffer.getName());
    }

    @Test
    public void testAddTag() {
        Offer actualOffer = offerService.create(testOffer);
        actualOffer = offerService.addTag(actualOffer, new Tag("added tag", offers));
        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("hot", offers));
        tags.add(new Tag("new", offers));
        tags.add(new Tag("added tag", offers));
        assertEquals(tags, actualOffer.getTags());
    }

    @Test
    public void testUpdateCategory() {
        Offer actualOffer = offerService.create(testOffer);

        Set<Offer> offers1 = new HashSet<>();
        offers1.add(actualOffer);

        Category category = new Category("updated category", offers1);
        Category expectedCategory = new Category("updated category", offers1);
        actualOffer = offerService.updateCategory(actualOffer, category);

        assertNotNull(actualOffer.getId());
        assertEquals(expectedCategory, actualOffer.getCategory());
        System.out.println("----------" + actualOffer);
    }

    @Test
    public void testDeleteSingleOffer() {
        Offer expectedOffer = offerService.create(testOffer);
        Long id = expectedOffer.getId();

        offerService.delete(id);

        Offer deletedOffer = offerService.find(id);

        assertNull(deletedOffer);
    }
}
