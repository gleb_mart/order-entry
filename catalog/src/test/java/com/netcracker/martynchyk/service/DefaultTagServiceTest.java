package com.netcracker.martynchyk.service;

import com.netcracker.martynchyk.config.*;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.services.TagService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@ContextConfiguration(classes = {DefaultTagServiceConfig.class, DefaultOfferServiceConfig.class})
public class DefaultTagServiceTest {

    @Autowired
    private TagService tagService;

    private ApplicationContext offerContext;
    private ApplicationContext categoryContext;
    private ApplicationContext tagContext;
    private Tag testTag;
    private Tag testTag1;

    @Before
    public void setUp() {
        offerContext = new AnnotationConfigApplicationContext(OfferConfig.class);
        categoryContext = new AnnotationConfigApplicationContext(CategoryConfig.class);
        tagContext = new AnnotationConfigApplicationContext(TagConfig.class);

        Offer testOffer= offerContext.getBean("offerA",Offer.class);
        Offer testOffer1= offerContext.getBean("offerB",Offer.class);

        Set<Offer> offers = new HashSet<>();
        offers.add(testOffer);
        offers.add(testOffer1);

        Category category = categoryContext.getBean("categoryA",Category.class);
        category.setCategory("shooter");
        category.setOffer(offers);

        testTag =tagContext.getBean("tagA",Tag.class);
        testTag1 =tagContext.getBean("tagB",Tag.class);

        testOffer.setName("COD");
        testOffer.setPrice(new BigDecimal(60));
        testOffer.setCategory(category);
        testOffer.setTags(null);

        testOffer1.setName("COD");
        testOffer1.setPrice(new BigDecimal(60));
        testOffer1.setCategory(category);
        testOffer1.setTags(null);

        testTag.setName("hot");
        testTag.setOffers(offers);
        testTag1.setName("new");
        testTag1.setOffers(offers);
    }

    @Test
    public void testCreateOffer() {
        Tag expectedTag = tagService.create(testTag);
        assertNotNull(expectedTag.getId());
    }

    @Test
    public void testFindAllOffer() {
        Tag expectedTag = tagService.create(testTag);
        Tag expectedOffer1 = tagService.create(testTag1);

        List<Tag> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedTag);
        expectedOffers.add(expectedOffer1);

        List<Tag> actualOffers = tagService.findAll();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }

    @Test
    public void testFindOffer() {
        Tag expectedTag = tagService.create(testTag);

        Tag actualOffer = tagService.find(testTag.getId());

        assertNotNull(actualOffer.getId());
        assertEquals(expectedTag.getId(), actualOffer.getId());
    }

    @Test
    public void testUpdateSingleOffer() {
        Tag expectedTag = tagService.create(testTag);
        expectedTag.setName("updated tag");

        Tag updatedTag = tagService.update(expectedTag);

        assertNotNull(updatedTag.getId());
        assertEquals(expectedTag.getId(), updatedTag.getId());
        assertEquals("updated tag", updatedTag.getName());
    }

    @Test
    public void testDeleteSingleOffer() {
        Tag expectedTag = tagService.create(testTag);
        Long id = expectedTag.getId();

        tagService.delete(id);

        Tag deletedOffer = tagService.find(id);

        assertNull(deletedOffer);
    }

    @Test
    public void testCreateSeveralTags() {
        List<Tag> testTags = new ArrayList<>();
        testTags.add(testTag);
        testTags.add(testTag1);
        List<Tag> expectedTags = tagService.createSeveralTags(testTags);
        for (Tag expectedTag : expectedTags) {
            assertNotNull(expectedTag.getId());
        }
    }
}
