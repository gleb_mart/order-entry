package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.dao.repositories.CategoryRepository;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.dao.implementation.DefaultCategoryDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class DefaultCategoryDaoTest {

    @Autowired
    private CategoryRepository categoryRepository;
    private DefaultCategoryDao categoryDao;
    private Category testCategory;
    private Category testCategory1;
    private Offer testOffer;
    private Offer testOffer1;
    private Set<Offer> offers;

    @Before
    public void setUp() {

        categoryDao = new DefaultCategoryDao(categoryRepository);

        offers = new HashSet<>();
        offers.add(testOffer);
        offers.add(testOffer1);

        testCategory = new Category("shooter", offers);
        testCategory1 = new Category("strategy", offers);

        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("hot", offers));
        tags.add(new Tag("new", offers));

        testOffer = new Offer("COD", new BigDecimal(60),
                new Category("shooters", offers), tags);

        testOffer1 = new Offer("Total War", new BigDecimal(30),
                new Category("strategy", offers), tags);
    }

    @Test
    public void testCreateCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        assertNotNull(expectedCategory.getId());
    }

    @Test
    public void testFindAllCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        Category expectedCategory1 = categoryDao.create(testCategory1);

        List<Category> expectedCategories = new ArrayList<>();
        expectedCategories.add(expectedCategory);
        expectedCategories.add(expectedCategory1);

        List<Category> actualCategories = categoryDao.findAll();

        assertNotNull(actualCategories);
        assertEquals(expectedCategories, actualCategories);
    }

    @Test
    public void testFindCategory() {
        Category expectedCategory = categoryDao.create(testCategory);

        Category actualCategory = categoryDao.find(testCategory.getId());

        assertNotNull(actualCategory.getId());
        assertEquals(expectedCategory.getId(), actualCategory.getId());
    }

    @Test
    public void testUpdateSingleCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        expectedCategory.setCategory("updated category");

        Category updatedCategory = categoryDao.update(expectedCategory);

        assertNotNull(updatedCategory.getId());
        assertEquals(expectedCategory.getId(), updatedCategory.getId());
        assertEquals("updated category", updatedCategory.getCategory());
    }

    @Test
    public void testDeleteSingleCategory() {
        Category expectedCategory = categoryDao.create(testCategory);
        Long id = expectedCategory.getId();

        categoryDao.delete(id);

        Category deletedCategory = categoryDao.find(id);

        assertNull(deletedCategory);
    }

    @Test
    public void testCreateSeveralCategories() {
        List<Category> testCategories = new ArrayList<>();
        testCategories.add(testCategory);
        testCategories.add(testCategory1);
        List<Category> expectedCategories = categoryDao.createSeveralCategories(testCategories);
        for (Category expectedCategory : expectedCategories) {
            assertNotNull(expectedCategory.getId());
        }
    }

}
