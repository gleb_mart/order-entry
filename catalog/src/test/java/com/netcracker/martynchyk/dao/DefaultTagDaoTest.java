package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.dao.implementation.DefaultTagDao;
import com.netcracker.martynchyk.dao.repositories.TagRepository;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class DefaultTagDaoTest {

    @Autowired
    private TagRepository tagRepository;
    private DefaultTagDao tagDao;
    private Tag testTag;
    private Tag testTag1;

    @Before
    public void setUp() {
        tagDao = new DefaultTagDao(tagRepository);

        Offer testOffer = new Offer("COD", new BigDecimal(60),
                new Category("shooter", null), null);
        Offer testOffer1 = new Offer("COD", new BigDecimal(60),
                new Category("shooter", null), null);

        Set<Offer> offers = new HashSet<>();
        offers.add(testOffer);
        offers.add(testOffer1);

        testTag = new Tag("hot", offers);
        testTag1 = new Tag("new", offers);
    }

    @Test
    public void testCreateOffer() {
        Tag expectedTag = tagDao.create(testTag);
        assertNotNull(expectedTag.getId());
    }

    @Test
    public void testFindAllOffer() {
        Tag expectedTag = tagDao.create(testTag);
        Tag expectedOffer1 = tagDao.create(testTag1);

        List<Tag> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedTag);
        expectedOffers.add(expectedOffer1);

        List<Tag> actualOffers = tagDao.findAll();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }

    @Test
    public void testFindOffer() {
        Tag expectedTag = tagDao.create(testTag);

        Tag actualOffer = tagDao.find(testTag.getId());

        assertNotNull(actualOffer.getId());
        assertEquals(expectedTag.getId(), actualOffer.getId());
    }

    @Test
    public void testUpdateSingleOffer() {
        Tag expectedTag = tagDao.create(testTag);
        expectedTag.setName("updated tag");

        Tag updatedTag = tagDao.update(expectedTag);

        assertNotNull(updatedTag.getId());
        assertEquals(expectedTag.getId(), updatedTag.getId());
        assertEquals("updated tag", updatedTag.getName());
    }

    @Test
    public void testDeleteSingleOffer() {
        Tag expectedTag = tagDao.create(testTag);
        Long id = expectedTag.getId();

        tagDao.delete(id);

        Tag deletedOffer = tagDao.find(id);

        assertNull(deletedOffer);
    }

    @Test
    public void testCreateSeveralTags() {
        List<Tag> testTags = new ArrayList<>();
        testTags.add(testTag);
        testTags.add(testTag1);
        List<Tag> expectedTags = tagDao.createSeveralTags(testTags);
        for (Tag expectedTag : expectedTags) {
            assertNotNull(expectedTag.getId());
        }
    }
}
