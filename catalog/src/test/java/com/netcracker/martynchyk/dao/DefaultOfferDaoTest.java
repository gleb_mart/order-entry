package com.netcracker.martynchyk.dao;


import com.netcracker.martynchyk.dao.repositories.CategoryRepository;
import com.netcracker.martynchyk.dao.repositories.OfferRepository;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.dao.implementation.DefaultOfferDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class DefaultOfferDaoTest {

    @Autowired
    private OfferRepository offerRepository;
    private DefaultOfferDao offerDao;
    private Offer testOffer;
    private Offer testOffer1;
    private Set<Offer> offers;

    @Before
    public void setUp() {
        offerDao = new DefaultOfferDao(offerRepository);

        offers = new HashSet<>();
        offers.add(testOffer);
        offers.add(testOffer1);

        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("hot", offers));
        tags.add(new Tag("new", offers));

        testOffer = new Offer("COD", new BigDecimal(60),
                new Category("shooter", offers), tags);

        testOffer1 = new Offer("Total War", new BigDecimal(30),
                new Category("strategy", offers), tags);

    }

    @Test
    public void testCreateOffer() {
        Offer expectedOffer = offerDao.create(testOffer);
        assertNotNull(expectedOffer.getId());
    }

    @Test
    public void testFindAllOffer() {
        Offer expectedOffer = offerDao.create(testOffer);
        Offer expectedOffer1 = offerDao.create(testOffer1);

        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedOffer);
        expectedOffers.add(expectedOffer1);

        List<Offer> actualOffers = offerDao.findAll();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }

    @Test
    public void testFindAllByPrice() {
        Offer expectedOffer = offerDao.create(testOffer);
        Offer expectedOffer1 = offerDao.create(testOffer1);

        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedOffer1);
        expectedOffers.add(expectedOffer);

        List<Offer> actualOffers = offerDao.findAllByPrice();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }

    @Test
    public void testFindAllByCategory() {
        Offer expectedOffer = offerDao.create(testOffer);
        Offer expectedOffer1 = offerDao.create(testOffer1);

        List<Offer> expectedOffers = new ArrayList<>();
        expectedOffers.add(expectedOffer1);
        expectedOffers.add(expectedOffer);

        List<Offer> actualOffers = offerDao.findAllByCategory();

        assertNotNull(actualOffers);
        assertEquals(expectedOffers, actualOffers);
    }


    @Test
    public void testFindOffer() {
        Offer expectedOffer = offerDao.create(testOffer);

        Offer actualOffer = offerDao.find(testOffer.getId());

        assertNotNull(actualOffer.getId());
        assertEquals(expectedOffer.getId(), actualOffer.getId());
    }

    @Test
    public void testUpdateSingleOffer() {
        Offer expectedOffer = offerDao.create(testOffer);
        expectedOffer.setName("category");

        Offer updatedOffer = offerDao.update(expectedOffer);

        assertNotNull(updatedOffer.getId());
        assertEquals(expectedOffer.getId(), updatedOffer.getId());
        assertEquals("category", updatedOffer.getName());
    }

    @Test
    public void testAddTag() {
        Offer expectedOffer = offerDao.create(testOffer);
        expectedOffer = offerDao.addTag(expectedOffer, new Tag("added tag", offers));
        Set<Tag> tags = new HashSet<>();
        tags.add(new Tag("hot", offers));
        tags.add(new Tag("new", offers));
        tags.add(new Tag("added tag", offers));
        assertEquals(expectedOffer.getTags(), tags);
    }

    @Test
    public void testUpdateCategory() {
        Offer actualOffer = offerDao.create(testOffer);

        Set<Offer> offers1 = new HashSet<>();
        offers1.add(actualOffer);

        Category category = new Category("updated category", offers1);
        Category expectedCategory = new Category("updated category", offers1);
        actualOffer = offerDao.updateCategory(actualOffer, category);

        assertNotNull(actualOffer.getId());
        assertEquals(expectedCategory, actualOffer.getCategory());
        System.out.println("----------" + actualOffer);
    }

    @Test
    public void testDeleteSingleOffer() {
        Offer expectedOffer = offerDao.create(testOffer);
        Long id = expectedOffer.getId();

        offerDao.delete(id);

        Offer deletedOffer = offerDao.find(id);

        assertNull(deletedOffer);
    }
}
