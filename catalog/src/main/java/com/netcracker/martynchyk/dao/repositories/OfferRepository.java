package com.netcracker.martynchyk.dao.repositories;

import com.netcracker.martynchyk.entity.Offer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository extends CrudRepository<Offer, Long> {
        List<Offer> findAllByOrderByPrice();

        @Query("SELECT offer FROM Offer offer ORDER BY offer.tags.size")
        List<Offer> findAllByTag();

        @Query("SELECT offer FROM Offer offer ORDER BY offer.category.category")
        List<Offer> findAllByCategory();
}
