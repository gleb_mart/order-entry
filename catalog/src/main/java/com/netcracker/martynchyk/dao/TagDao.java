package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Tag;

import java.util.List;
import java.util.Set;

public interface TagDao {

    Tag create(Tag tag);

    List<Tag> findAll();

    Tag find(long id);

    Tag findByName(String name);

    Set<Tag> findByName(Set<String> names);

    Tag update(Tag tag);

    void delete(long id);

    List<Tag> createSeveralTags(List<Tag> tags);

}
