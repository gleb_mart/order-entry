package com.netcracker.martynchyk.dao.repositories;

import com.netcracker.martynchyk.entity.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category,Long> {
}
