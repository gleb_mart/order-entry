package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.dao.OfferDao;
import com.netcracker.martynchyk.dao.repositories.OfferRepository;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DefaultOfferDao implements OfferDao {

    @Autowired
    private OfferRepository offerRepository;

    public DefaultOfferDao() {
    }

    public DefaultOfferDao(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Override
    public Offer create(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    public List<Offer> findAll() {
        List<Offer> offers = new ArrayList<>();
        offerRepository.findAll().forEach(offers::add);
        return offers;
    }

    @Override
    public List<Offer> findAllById(List<Long> ids) {
        List<Offer> offers = new ArrayList<>();
        offerRepository.findAllById(ids).forEach(offers::add);
        return offers;
    }

    @Override
    public List<Offer> findAllByTag() {
        return offerRepository.findAllByTag();
    }

    @Override
    public List<Offer> findAllByPrice() {
        return offerRepository.findAllByOrderByPrice();
    }

    @Override
    public List<Offer> findAllByCategory() {
        return offerRepository.findAllByCategory();
    }

    @Override
    public Offer find(long id) {
        return offerRepository.findById(id).orElse(null);
    }

    @Override
    public Offer update(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    public void delete(long id) {
        offerRepository.deleteById(id);
    }

    @Override
    public Offer addTag(Offer offer, Tag tag) {
        Set<Tag> tags = offer.getTags();
        tags.add(tag);
        offer.setTags(tags);
        return offerRepository.save(offer);
    }

    @Override
    public Offer updateCategory(Offer offer, Category category) {
        offer.setCategory(category);
        return offerRepository.save(offer);
    }
}
