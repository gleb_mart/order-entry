package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.dao.CategoryDao;
import com.netcracker.martynchyk.dao.repositories.CategoryRepository;
import com.netcracker.martynchyk.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class DefaultCategoryDao implements CategoryDao {

    @Autowired
    private CategoryRepository categoryRepository;

    public DefaultCategoryDao() {
    }

    public DefaultCategoryDao(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> findAll() {
        List<Category> categories = new ArrayList<>();
        categoryRepository.findAll().forEach(categories::add);
        return categories;
    }

    @Override
    public Category find(long id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public Category update(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public void delete(long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public List<Category> createSeveralCategories(List<Category> categories) {
        categoryRepository.saveAll(categories).forEach(categories::add);
        return categories;
    }
}
