package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.dao.TagDao;
import com.netcracker.martynchyk.dao.repositories.TagRepository;
import com.netcracker.martynchyk.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DefaultTagDao implements TagDao {

    @Autowired
    private TagRepository tagRepository;

    public DefaultTagDao() {
    }

    public DefaultTagDao(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag create(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public List<Tag> findAll() {
        List<Tag> tags = new ArrayList<>();
        tagRepository.findAll().forEach(tags::add);
        return tags;
    }

    @Override
    public Tag find(long id) {
        return tagRepository.findById(id).orElse(null);
    }

    @Override
    public Tag findByName(String name) {
        return tagRepository.findByName(name);
    }

    @Override
    public Set<Tag> findByName(Set<String> names) {
        //return tagRepository.findByTag(names);
        Set<Tag> tags = new HashSet<>();
        tagRepository.findByName(names).forEach(tags::add);
        return tags;
        //return null;
    }

    @Override
    public Tag update(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public void delete(long id) {
        tagRepository.deleteById(id);
    }

    @Override
    public List<Tag> createSeveralTags(List<Tag> tags) {
        tagRepository.saveAll(tags).forEach(tags::add);
        return tags;
    }
}
