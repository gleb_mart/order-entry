package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Category;

import java.util.List;

public interface CategoryDao {

    Category create(Category category);

    List<Category> findAll();

    Category find(long id);

    Category update(Category category);

    void delete(long id);

    List<Category> createSeveralCategories(List<Category> categories);
}
