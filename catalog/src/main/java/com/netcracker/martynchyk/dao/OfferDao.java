package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;

import java.util.List;

public interface OfferDao {

    Offer create(Offer offer);

    List<Offer> findAll();

    List<Offer> findAllById(List<Long> ids);

    List<Offer> findAllByTag();

    List<Offer> findAllByPrice();

    List<Offer> findAllByCategory();

    Offer find(long id);

    Offer update(Offer offer);

    void delete(long id);

    Offer addTag(Offer offer, Tag tag);

    Offer updateCategory(Offer offer, Category category);
}
