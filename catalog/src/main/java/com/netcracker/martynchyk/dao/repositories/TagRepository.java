package com.netcracker.martynchyk.dao.repositories;

import com.netcracker.martynchyk.entity.Tag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {
    Tag findByName(String name);

    Iterable<Tag> findByName(Iterable<String> name);
}
