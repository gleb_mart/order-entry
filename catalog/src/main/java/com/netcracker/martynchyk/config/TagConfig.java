package com.netcracker.martynchyk.config;


import com.netcracker.martynchyk.entity.Tag;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class TagConfig {

    @Bean
    public Tag tagA() {
        Tag tag = new Tag();
        tag.setName("new");
        return tag;
    }

    @Bean
    public Tag tagB() {
        Tag tag = new Tag();
        tag.setName("hot");
        return tag;
    }
}
