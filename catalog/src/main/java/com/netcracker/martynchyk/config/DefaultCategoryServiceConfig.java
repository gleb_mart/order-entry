package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.CategoryDao;
import com.netcracker.martynchyk.dao.implementation.DefaultCategoryDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class DefaultCategoryServiceConfig {

    @Bean
    public CategoryDao categoryDao() {
        return new DefaultCategoryDao();
    }
}
