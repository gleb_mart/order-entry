package com.netcracker.martynchyk.config;


import com.netcracker.martynchyk.entity.Offer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class OfferConfig {

    @Bean
    public Offer offerA() {
        Offer offer = new Offer();
        return offer;
    }

    @Bean
    public Offer offerB() {
        Offer offer = new Offer();
        return offer;
    }
}
