package com.netcracker.martynchyk.config;


import com.netcracker.martynchyk.entity.Category;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class CategoryConfig {

    @Bean
    public Category categoryA() {
        Category category = new Category();
        return category;
    }

    @Bean
    public Category categoryB() {
        Category category = new Category();
        return category;
    }
}
