package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.TagDao;
import com.netcracker.martynchyk.dao.implementation.DefaultTagDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class DefaultTagServiceConfig {

    @Bean
    public TagDao tagDao() {
        return new DefaultTagDao();
    }
}
