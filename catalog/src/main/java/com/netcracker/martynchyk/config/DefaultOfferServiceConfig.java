package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.OfferDao;
import com.netcracker.martynchyk.dao.implementation.DefaultOfferDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class DefaultOfferServiceConfig {

    @Bean
    public OfferDao offerDao() {
        return new DefaultOfferDao();
    }
}
