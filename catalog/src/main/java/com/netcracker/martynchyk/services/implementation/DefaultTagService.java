package com.netcracker.martynchyk.services.implementation;

import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.dao.TagDao;
import com.netcracker.martynchyk.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class DefaultTagService implements TagService {

    private final TagDao tagDao;

    @Autowired
    public DefaultTagService(TagDao tagDao) {
        this.tagDao = tagDao;
    }

    @Override
    public Tag create(Tag tag) {
        Tag foundTag=tagDao.findByName(tag.getName());
        if(foundTag!=null){
            return foundTag;
        }
        else {
            return tagDao.create(tag);
        }
    }

    @Override
    public Set<Tag> findExists(Set<Tag> tags){
        Tag foundTag;
        Set<Tag> newTags = new HashSet<>();
        for (Tag tag: tags) {
            foundTag = findByName(tag.getName());
            if (foundTag != null) {
                newTags.add(foundTag);
            }
            else {
                newTags.add(tag);
            }
        }
        return  newTags;
    }

    @Override
    public Tag find(long id) {
        return tagDao.find(id);
    }

    @Override
    public Tag findByName(String name) {
        return tagDao.findByName(name);
    }

    @Override
    public Set<Tag> findByName(Set<String> names) {
        return tagDao.findByName(names);
    }

    @Override
    public List<Tag> findAll() {
        return tagDao.findAll();
    }

    @Override
    public Tag update(Tag tag) {
        return tagDao.update(tag);
    }

    @Override
    public void delete(long id) {
        tagDao.delete(id);
    }

    @Override
    public List<Tag> createSeveralTags(List<Tag> tags) {
        return tagDao.createSeveralTags(tags);
    }
}
