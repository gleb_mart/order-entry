package com.netcracker.martynchyk.services.implementation;

import com.netcracker.martynchyk.dao.OfferDao;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.services.OfferService;
import com.netcracker.martynchyk.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultOfferService implements OfferService {

    private final OfferDao offerDao;

    @Autowired
    private TagService tagService;

    @Autowired
    public DefaultOfferService(OfferDao offerDao) {
        this.offerDao = offerDao;
    }

    @Override
    public Offer create(Offer offer) {
        offer.setTags(tagService.findExists(offer.getTags()));
        return offerDao.create(offer);
    }

    @Override
    public Offer find(long id) {
        return offerDao.find(id);
    }

    @Override
    public List<Offer> findAll() {
        return offerDao.findAll();
    }

    @Override
    public List<Offer> findAllById(List<Long> ids) {
        return offerDao.findAllById(ids);
    }

    @Override
    public Offer update(Offer offer) {
        return offerDao.update(offer);
    }

    @Override
    public void delete(long id) {
        offerDao.delete(id);
    }

    @Override
    public List<Offer> findAllByTag() {
        return offerDao.findAllByTag();
    }

    @Override
    public List<Offer> findAllByPrice() {
        return offerDao.findAllByPrice();
    }

    @Override
    public List<Offer> findAllByCategory() {
        return offerDao.findAllByCategory();
    }

    @Override
    public Offer addTag(Offer offer, Tag tag) {
        return offerDao.addTag(offer, tag);
    }

    @Override
    public Offer updateCategory(Offer offer, Category category) {
        return offerDao.updateCategory(offer, category);
    }
}
