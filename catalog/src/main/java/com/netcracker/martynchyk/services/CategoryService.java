package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.entity.Category;

import java.util.List;

public interface CategoryService {

    Category create(Category category);

    List<Category> findAll();

    Category find(long id);

    Category update(Category category);

    void delete(long id);

    List<Category> createSeveralCategories(List<Category> categories);
}
