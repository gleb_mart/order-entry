package com.netcracker.martynchyk.services.implementation;

import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.dao.CategoryDao;
import com.netcracker.martynchyk.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultCategoryService implements CategoryService {

    private final CategoryDao categoryDao;

    @Autowired
    public DefaultCategoryService(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public Category create(Category category) {
        return categoryDao.create(category);
    }

    @Override
    public Category find(long id) {
        return categoryDao.find(id);
    }

    @Override
    public List<Category> findAll() {
        return categoryDao.findAll();
    }

    @Override
    public Category update(Category category) {
        return categoryDao.update(category);
    }

    @Override
    public void delete(long id) {
        categoryDao.delete(id);
    }

    @Override
    public List<Category> createSeveralCategories(List<Category> categories) {
        return categoryDao.createSeveralCategories(categories);
    }
}
