package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.entity.Tag;

import java.util.List;
import java.util.Set;

public interface TagService {

    Tag create(Tag tag);

    Set<Tag> findExists(Set<Tag> tags);

    List<Tag> findAll();

    Tag find(long id);

    Tag findByName(String name);

    Set<Tag> findByName(Set<String> names);

    Tag update(Tag tag);

    void delete(long id);

    List<Tag> createSeveralTags(List<Tag> tags);
}
