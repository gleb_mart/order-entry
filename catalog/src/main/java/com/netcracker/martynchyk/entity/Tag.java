package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.TagDto;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Tag extends BaseEntity {

    @Basic(optional = false)
    private String name;

    @ManyToMany(mappedBy = "tags")
    private Set<Offer> offers = new HashSet<>();

    public Tag() {
    }

    public Tag(String tag, Set<Offer> offers) {
        this.name = tag;
        this.offers = offers;
    }

    public String getName() {
        return name;
    }

    public void setName(String tag) {
        this.name = tag;
    }

    public Set<Offer> getOffers() {
        return offers;
    }

    public void setOffers(Set<Offer> offers) {
        this.offers = offers;
    }

    public static Tag convertToEntity(TagDto tagDto){
        Tag tag = new Tag();
        tag.setName(tagDto.getName());
        tag.setOffers(tagDto.getOffers());
        return tag;
    }

    public TagDto convertToDto(){
        return new TagDto(name,offers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag1 = (Tag) o;
        return Objects.equals(name, tag1.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tag='" + name + '\'' +
                '}';
    }
}
