package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.OfferDto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Offer extends BaseEntity {


    @Basic(optional = false)
    private String name;

    @Basic(optional = false)
    private BigDecimal price;

    @ManyToOne()
    private Category category;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "offer_tag",
            joinColumns = @JoinColumn(name = "offer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new HashSet<>();

    public Offer() {
    }

    public Offer(String name, BigDecimal price, Category category, Set<Tag> tags) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public static Offer convertToEntity(OfferDto offerDto) {
        Offer offer = new Offer();
        offer.setPrice(offerDto.getPrice());
        offer.setName(offerDto.getName());
        offer.setTags(offerDto.getTagsEntity());
        return offer;
    }

    public OfferDto convertToDto() {
        return new OfferDto(name, price, category.getId(), tags);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offer offer = (Offer) o;
        return Objects.equals(name, offer.name) &&
                Objects.equals(price, offer.price) &&
                Objects.equals(category, offer.category) &&
                Objects.equals(tags, offer.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, category, tags);
    }

    @Override
    public String toString() {
        return "OfferRepository{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", category=" + category +
                ", tags=" + tags +
                '}';
    }
}
