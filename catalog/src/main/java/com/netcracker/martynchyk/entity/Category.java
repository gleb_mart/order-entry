package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.CategoryDto;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Category extends BaseEntity {

    @Basic(optional = false)
    private String category;

    @OneToMany(mappedBy = "category",cascade = CascadeType.ALL)
    private Set<Offer> offer = new HashSet<>();

    public Category() {
    }

    public Category(String category, Set<Offer> offer) {
        this.category = category;
        this.offer = offer;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Set<Offer> getOffer() {
        return offer;
    }

    public void setOffer(Set<Offer> offer) {
        this.offer = offer;
    }

    public static Category convertToEntity(CategoryDto categoryDto){
        return new Category(categoryDto.getCategory(),categoryDto.getOfferEntity());
    }

    public CategoryDto convertToDto(){
        return new CategoryDto(category,offer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category1 = (Category) o;
        return Objects.equals(category, category1.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category);
    }

    @Override
    public String toString() {
        return "Category{" +
                "category='" + category + '\'' +
                '}';
    }
}
