package com.netcracker.martynchyk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netcracker.martynchyk.entity.Offer;

import java.util.Set;
import java.util.stream.Collectors;

public class CategoryDto {

    private String category;

    private Set<Offer> offers;

    public CategoryDto(String category, Set<Offer> offers) {
        this.category = category;
        this.offers = offers;
    }

    public String getCategory() {
        return category;
    }

    @JsonIgnore
    public Set<Offer> getOfferEntity(){
        return offers;
    }

    @JsonIgnore
    public Set<OfferDto> getOffer() {
        return offers.stream()
                .map(offer ->offer.convertToDto() )
                .collect(Collectors.toSet());
    }
}
