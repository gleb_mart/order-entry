package com.netcracker.martynchyk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netcracker.martynchyk.entity.Offer;

import java.util.Set;

public class TagDto {
    private String name;

    private Set<Offer> offers;

    public TagDto(String name, Set<Offer> offers) {
        this.name = name;
        this.offers = offers;
    }

    public String getName() {
        return name;
    }

    @JsonIgnore
    public Set<Offer> getOffers() {
        return offers;
    }
}
