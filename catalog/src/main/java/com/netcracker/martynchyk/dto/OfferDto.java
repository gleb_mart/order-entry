package com.netcracker.martynchyk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netcracker.martynchyk.entity.Tag;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;

public class OfferDto {

    private String name;

    private BigDecimal price;

    private Long category;

    private Set<Tag> tags;

    public OfferDto(String name, BigDecimal price, Long category, Set<Tag> tags) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Long getCategory(){
        return category;
    }

    @JsonIgnore
    public Set<Tag> getTagsEntity() {
        return tags;
    }

    public Set<TagDto> getTags() {
        return tags.stream()
                .map(tag -> tag.convertToDto())
                .collect(Collectors.toSet());
    }
}
