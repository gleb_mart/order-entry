package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.CategoryDto;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.services.OfferService;
import com.netcracker.martynchyk.services.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/catalog/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    private OfferService offerService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation(value = "Return category by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryDto get(
            @ApiParam(value = "Id of category to lookup for", required = true)
            @PathVariable("id") Long id) {
        return categoryService.find(id).convertToDto();
    }

    @ApiOperation(value = "Return all categories")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryDto> getAll() {
        List<Category> categorys = categoryService.findAll();
        return categorys.stream()
                .map(category -> category.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create category")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CategoryDto createCategoryItem(@RequestBody CategoryDto categoryDto) {
        Category category = Category.convertToEntity(categoryDto);
        Category categoryCreated = categoryService.create(category);
        return categoryCreated.convertToDto();
    }

    @ApiOperation(value = "Update category by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public CategoryDto updateCategoryItem(
            @PathVariable("id") Long id,
            @RequestBody CategoryDto categoryDto) {
        Category category = categoryService.find(id);
        category.setCategory(categoryDto.getCategory());
        return categoryService.update(category).convertToDto();
    }

    @ApiOperation(value = "Delete category by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategoryItem(@PathVariable("id") Long id) {
        categoryService.delete(id);
    }
}
