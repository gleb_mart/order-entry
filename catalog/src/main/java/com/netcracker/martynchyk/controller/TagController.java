package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.TagDto;
import com.netcracker.martynchyk.entity.Tag;
import com.netcracker.martynchyk.services.OfferService;
import com.netcracker.martynchyk.services.TagService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/catalog/tags")
public class TagController {

    private final TagService tagService;

    @Autowired
    private OfferService offerService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @ApiOperation(value = "Return tag by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TagDto get(
            @ApiParam(value = "Id of tag to lookup for", required = true)
            @PathVariable("id") Long id) {
        return tagService.find(id).convertToDto();
    }

    @ApiOperation(value = "Return all tags")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<TagDto> getAll() {
        List<Tag> tags = tagService.findAll();
        return tags.stream()
                .map(tag -> tag.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create tag")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TagDto createTagItem(@RequestBody TagDto tagDto) {
        Tag tag = Tag.convertToEntity(tagDto);
        Tag tagCreated = tagService.create(tag);
        return tagCreated.convertToDto();
    }

    @ApiOperation(value = "Update tag by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TagDto updateTagItem(
            @PathVariable("id") Long id,
            @RequestBody TagDto tagDto) {
        Tag tag = tagService.find(id);
        tag.setName(tagDto.getName());
        return tagService.update(tag).convertToDto();
    }

    @ApiOperation(value = "Delete tag by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTagItem(@PathVariable("id") Long id) {
        tagService.delete(id);
    }
}
