package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.OfferDto;
import com.netcracker.martynchyk.entity.Category;
import com.netcracker.martynchyk.entity.Offer;
import com.netcracker.martynchyk.exceptions.ThereIsNoSuchContentException;
import com.netcracker.martynchyk.services.CategoryService;
import com.netcracker.martynchyk.services.OfferService;
import com.netcracker.martynchyk.services.TagService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/catalog/offers")
public class OfferController {

    private final OfferService offerService;

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TagService tagService;

    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @ApiOperation(value = "Return offer by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OfferDto get(
            @ApiParam(value = "Id of offer to lookup for", required = true)
            @PathVariable("id") Long id) {
        OfferDto foundOffer=offerService.find(id).convertToDto();
        return foundOffer;
    }

    @ApiOperation(value = "Return all offers")
    @GetMapping(path = "/view")
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.OK)
    public List<OfferDto> getAll() {
        List<Offer> offers = offerService.findAll();
        return offers.stream()
                .map(offer -> offer.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Return all offers by ids")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<OfferDto> getAllById(
            @ApiParam(value = "Ids of offer to lookup for", required = true)
            @RequestParam(value = "ids") List<Long> ids) {
        List<Offer> offers = offerService.findAllById(ids);
        return offers.stream()
                .map(offer -> offer.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create offer")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OfferDto createOfferItem(@RequestBody OfferDto offerDto) {
        Offer offer = Offer.convertToEntity(offerDto);
        Category findCategory = categoryService.find(offerDto.getCategory());
        offer.setCategory(findCategory);
        return offerService.create(offer).convertToDto();
    }

    @ApiOperation(value = "Update offer by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OfferDto updateOfferItem(
            @PathVariable("id") Long id,
            @RequestBody OfferDto offerDto) {
        Offer offer = offerService.find(id);
        offer.setName(offerDto.getName());
        offer.setPrice(offerDto.getPrice());
        offer.setTags(tagService.findExists(offer.getTags()));
        if (offerDto.getCategory() != offer.getCategory().getId()) {
            Category findCategory = categoryService.find(offerDto.getCategory());
            offer.setCategory(findCategory);
        }
        return offerService.update(offer).convertToDto();
    }

    @ApiOperation(value = "Delete offer by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOfferItem(@PathVariable("id") Long id) {
        offerService.delete(id);
    }
}
