package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.dao.implementation.DefaultOrderDao;
import com.netcracker.martynchyk.dao.repositories.OrderRepository;
import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.entity.OrderItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class DefaultOrderDaoTest {

    @Autowired
    private OrderRepository orderRepository;
    private DefaultOrderDao orderDao;

    private Order testOrder;
    private Order testOrder1;

    @Before
    public void setUp() {
        orderDao = new DefaultOrderDao(orderRepository);
        testOrder = new Order();
        testOrder1 = new Order();

        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(new OrderItem((long) 00, testOrder));
        orderItems.add(new OrderItem((long) 11, testOrder));


        List<OrderItem> orderItems1 = new ArrayList<>();
        orderItems1.add(new OrderItem((long) 22, testOrder1));
        orderItems1.add(new OrderItem((long) 33, testOrder1));

        testOrder.setOrderItems(orderItems);
        testOrder.setCustomer((long) 0);
        testOrder.setTotalPrice(new BigDecimal(300));
        testOrder.setTotalAmount(15);
        testOrder.setDate(2000);
        testOrder.setPaymentStatus(false);


        testOrder1.setOrderItems(orderItems1);
        testOrder1.setCustomer((long) 1);
        testOrder1.setTotalPrice(new BigDecimal(100));
        testOrder1.setTotalAmount(1);
        testOrder1.setDate(1998);
        testOrder1.setPaymentStatus(true);
    }

    @Test
    public void testCreateOrder() {
        Order createdOrder = orderDao.create(testOrder);
        assertNotNull(createdOrder.getId());
    }

    @Test
    public void testFindAllOrder() {
        Order createdOrder = orderDao.create(testOrder);

        List<Order> createdOrders = new ArrayList<>();
        createdOrders.add(createdOrder);

        List<Order> foundOrders = orderDao.findAll();

        assertNotNull(foundOrders);
        assertEquals(createdOrders, foundOrders);
    }

    @Test
    public void testFindOrder() {
        Order createdOrder = orderDao.create(testOrder);

        Order foundOrder = orderDao.find(testOrder.getId());

        assertNotNull(foundOrder.getId());
        assertEquals(createdOrder.getId(), foundOrder.getId());
    }

    @Test
    public void testUpdateSingleUser() {
        Order createdOrder = orderDao.create(testOrder);
        createdOrder.setPaymentStatus(true);

        Order updatedOrder = orderDao.update(createdOrder);

        assertNotNull(updatedOrder.getId());
        assertEquals(createdOrder.getId(), updatedOrder.getId());

        assertEquals(true, updatedOrder.isPaymentStatus());
    }

    @Test
    public void testDeleteSingleUser() {
        Order createdOrder = orderDao.create(testOrder);
        Long id = createdOrder.getId();

        orderDao.delete(id);

        Order deletedOrder = orderDao.find(id);

        assertNull(deletedOrder);
    }
}
