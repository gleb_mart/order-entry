package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.dao.implementation.DefaultOrderItemDao;
import com.netcracker.martynchyk.dao.repositories.OrderItemRepository;
import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.entity.OrderItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
public class DefaultOrderItemDaoTest {

    private DefaultOrderItemDao orderItemDao;

    @Autowired
    private OrderItemRepository orderItemRepository;


    private OrderItem orderItem;
    private OrderItem orderItem1;
    private Order testOrder;
    private Order testOrder1;

    @Before
    @Rollback(value = false)
    public void setUp() {

        orderItemDao = new DefaultOrderItemDao(orderItemRepository);

        testOrder = new Order();
        testOrder1 = new Order();

        orderItem = new OrderItem((long) 00, testOrder);
        orderItem1 = new OrderItem((long) 11, testOrder1);

        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);
        orderItems.add(orderItem1);

        testOrder.setOrderItems(orderItems);
        testOrder.setCustomer((long) 0);
        testOrder.setTotalPrice(new BigDecimal(300));
        testOrder.setTotalAmount(15);
        testOrder.setDate(2000);
        testOrder.setPaymentStatus(false);

        testOrder1.setOrderItems(orderItems);
        testOrder1.setCustomer((long) 1);
        testOrder1.setTotalPrice(new BigDecimal(100));
        testOrder1.setTotalAmount(1);
        testOrder1.setDate(1998);
        testOrder1.setPaymentStatus(true);
    }

    @Test
    @Rollback(value = true)
    public void testCreateOrder() {
        OrderItem expectedOrderItems = orderItemDao.create(orderItem);
        assertNotNull(expectedOrderItems.getId());

    }

    @Test
    public void testFindAllOrder() {
        OrderItem expectedOrderItem = orderItemDao.create(orderItem);
        OrderItem expectedOrderItem1 = orderItemDao.create(orderItem1);

        List<OrderItem> expectedOrderItems = new ArrayList<>();
        expectedOrderItems.add(expectedOrderItem1);
        expectedOrderItems.add(expectedOrderItem);

        List<OrderItem> actualOrderItems = orderItemDao.findAll();

        assertNotNull(actualOrderItems);
        assertEquals(expectedOrderItems, actualOrderItems);
        System.out.println("order--- " + actualOrderItems);
    }

    @Test
    public void testFindOrder() {
        OrderItem expectedOrderItems = orderItemDao.create(orderItem);

        OrderItem actualOrderItems = orderItemDao.find(orderItem.getId());

        assertNotNull(actualOrderItems.getId());
        assertEquals(expectedOrderItems.getId(), actualOrderItems.getId());
    }

    @Test
    public void testUpdateSingleUser() {
        OrderItem expectedOrderItems = orderItemDao.create(orderItem);
        expectedOrderItems.setOrder(testOrder1);

        OrderItem updatedOrder = orderItemDao.update(expectedOrderItems);

        assertNotNull(updatedOrder.getId());
        assertEquals(expectedOrderItems.getId(), updatedOrder.getId());

        assertEquals(testOrder1, updatedOrder.getOrder());
    }

    @Test
    public void testDeleteSingleUser() {
        OrderItem expectedOrderItems = orderItemDao.create(orderItem);
        Long id = expectedOrderItems.getId();

        orderItemDao.delete(id);

        OrderItem deletedOrderItem = orderItemDao.find(id);

        assertNull(deletedOrderItem);
    }
}

