package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.config.DefaultOrderItemServiceConfig;
import com.netcracker.martynchyk.config.OrderConfig;
import com.netcracker.martynchyk.config.OrderItemConfig;
import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.entity.OrderItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@ContextConfiguration(classes = {DefaultOrderItemServiceConfig.class})
public class DefaultOrderItemServiceTest {

    @Autowired
    private OrderItemService orderItemService;

    private ApplicationContext orderContext;
    private ApplicationContext orderItemContext;
    private OrderItem orderItem;
    private OrderItem orderItem1;
    private Order testOrder;
    private Order testOrder1;

    @Before
    public void setUp() {
        orderContext = new AnnotationConfigApplicationContext(OrderConfig.class);
        orderItemContext = new AnnotationConfigApplicationContext(OrderItemConfig.class);
        testOrder = orderContext.getBean("orderA", Order.class);
        testOrder1 = orderContext.getBean("orderB", Order.class);

        orderItem = orderItemContext.getBean("orderItemA", OrderItem.class);
        orderItem1 = orderItemContext.getBean("orderItemB", OrderItem.class);

        orderItem.setOrder(testOrder);
        orderItem1.setOrder(testOrder1);

        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);
        orderItems.add(orderItem1);

        testOrder.setOrderItems(orderItems);

        testOrder1.setOrderItems(orderItems);
    }

    @Test
    public void testCreateOrder() {

        OrderItem expectedOrderItems = orderItemService.create(orderItem);
        assertNotNull(expectedOrderItems.getId());
    }

    @Test
    public void testFindAllOrder() {
        OrderItem expectedOrderItem = orderItemService.create(orderItem);
        OrderItem expectedOrderItem1 = orderItemService.create(orderItem1);

        List<OrderItem> expectedOrderItems = new ArrayList<>();
        expectedOrderItems.add(expectedOrderItem1);
        expectedOrderItems.add(expectedOrderItem);

        List<OrderItem> actualOrderItems = orderItemService.findAll();

        assertNotNull(actualOrderItems);
        assertEquals(expectedOrderItems, actualOrderItems);
    }

    @Test
    public void testFindOrder() {
        OrderItem expectedOrderItems = orderItemService.create(orderItem);

        OrderItem actualOrderItems = orderItemService.find(orderItem.getId());

        assertNotNull(actualOrderItems.getId());
        assertEquals(expectedOrderItems.getId(), actualOrderItems.getId());
    }

    @Test
    public void testUpdateSingleUser() {
        OrderItem expectedOrderItems = orderItemService.create(orderItem);
        expectedOrderItems.setOrder(testOrder1);

        OrderItem updatedOrder = orderItemService.update(expectedOrderItems);

        assertNotNull(updatedOrder.getId());
        assertEquals(expectedOrderItems.getId(), updatedOrder.getId());

        assertEquals(testOrder1, updatedOrder.getOrder());
    }

    @Test
    public void testDeleteSingleUser() {
        OrderItem expectedOrderItems = orderItemService.create(orderItem);
        Long id = expectedOrderItems.getId();

        orderItemService.delete(id);

        OrderItem deletedOrderItem = orderItemService.find(id);

        assertNull(deletedOrderItem);
    }
}

