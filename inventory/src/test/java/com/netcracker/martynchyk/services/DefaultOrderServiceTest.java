package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.config.DefaultOrderServiceConfig;
import com.netcracker.martynchyk.config.OrderConfig;
import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.entity.OrderItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
//@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DefaultOrderServiceConfig.class})
@Import(DefaultOrderServiceConfig.class)
public class DefaultOrderServiceTest {

    @Autowired
    private OrderService orderService;

    private ApplicationContext orderContext;
    private Order testOrder;
    private Order testOrder1;

    @Before
    public void setUp() {
        orderContext = new AnnotationConfigApplicationContext(OrderConfig.class);
        testOrder = orderContext.getBean("orderA", Order.class);
        testOrder1 = orderContext.getBean("orderB", Order.class);

        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(new OrderItem((long) 00, testOrder));
        orderItems.add(new OrderItem((long) 11, testOrder));


        List<OrderItem> orderItems1 = new ArrayList<>();
        orderItems1.add(new OrderItem((long) 22, testOrder1));
        orderItems1.add(new OrderItem((long) 33, testOrder1));

        testOrder.setOrderItems(orderItems);
        testOrder1.setOrderItems(orderItems1);
    }

    @Test
    public void testCreateOrder() {
        Order createdOrder = orderService.create(testOrder);
        assertNotNull(createdOrder.getId());
    }

    @Test
    public void testFindAllOrder() {
        Order createdOrder = orderService.create(testOrder);

        List<Order> createdOrders = new ArrayList<>();
        createdOrders.add(createdOrder);

        List<Order> foundOrders = orderService.findAll();

        assertNotNull(foundOrders);
        assertEquals(createdOrders, foundOrders);
    }

    @Test
    public void testFindOrder() {
        Order createdOrder = orderService.create(testOrder);

        Order foundOrder = orderService.find(testOrder.getId());

        assertNotNull(foundOrder.getId());
        assertEquals(createdOrder.getId(), foundOrder.getId());
    }

    @Test
    public void testUpdateSingleUser() {
        Order createdOrder = orderService.create(testOrder);
        createdOrder.setPaymentStatus(true);

        Order updatedOrder = orderService.update(createdOrder);

        assertNotNull(updatedOrder.getId());
        assertEquals(createdOrder.getId(), updatedOrder.getId());

        assertEquals(true, updatedOrder.isPaymentStatus());
    }

    @Test
    public void testDeleteSingleUser() {
        Order createdOrder = orderService.create(testOrder);
        Long id = createdOrder.getId();

        orderService.delete(id);

        Order deletedOrder = orderService.find(id);

        assertNull(deletedOrder);
    }
}
