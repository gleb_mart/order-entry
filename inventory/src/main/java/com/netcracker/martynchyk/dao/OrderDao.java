package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.Order;

import java.util.List;

public interface OrderDao {

    Order create(Order order);

    List<Order> findAll();

    Order find(long id);

    Order update(Order order);

    void delete(long id);
}
