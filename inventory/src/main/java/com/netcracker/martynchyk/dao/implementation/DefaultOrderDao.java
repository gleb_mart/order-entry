package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.dao.OrderDao;
import com.netcracker.martynchyk.dao.repositories.OrderRepository;
import com.netcracker.martynchyk.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class DefaultOrderDao implements OrderDao {

    @Autowired
    private OrderRepository orderRepository;

    public DefaultOrderDao() {
    }

    public DefaultOrderDao(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order create(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Order> findAll() {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    @Override
    public Order find(long id) {
        return orderRepository.findById(id).orElse(null);
    }

    @Override
    public Order update(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void delete(long id) {
        orderRepository.deleteById(id);
    }
}
