package com.netcracker.martynchyk.dao.implementation;

import com.netcracker.martynchyk.dao.OrderItemDao;
import com.netcracker.martynchyk.dao.repositories.OrderItemRepository;
import com.netcracker.martynchyk.entity.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class DefaultOrderItemDao implements OrderItemDao {

    @Autowired
    OrderItemRepository orderItemRepository;

    public DefaultOrderItemDao() {
    }

    public DefaultOrderItemDao(OrderItemRepository orderItemRepository) {
        this.orderItemRepository = orderItemRepository;
    }

    @Override
    public OrderItem create(OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    @Override
    public List<OrderItem> findAll() {
        List<OrderItem> orderItems = new ArrayList<>();
        orderItemRepository.findAll().forEach(orderItems::add);
        return orderItems;
    }

    @Override
    public OrderItem find(long id) {
        return orderItemRepository.findById(id).orElse(null);
    }

    @Override
    public OrderItem update(OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    @Override
    public void delete(long id) {
        orderItemRepository.deleteById(id);
    }
}
