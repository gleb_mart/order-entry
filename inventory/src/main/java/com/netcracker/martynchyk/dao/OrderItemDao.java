package com.netcracker.martynchyk.dao;

import com.netcracker.martynchyk.entity.OrderItem;

import java.util.List;

public interface OrderItemDao {

    OrderItem create(OrderItem orderItem);

    List<OrderItem> findAll();

    OrderItem find(long id);

    OrderItem update(OrderItem orderItem);

    void delete(long id);
}
