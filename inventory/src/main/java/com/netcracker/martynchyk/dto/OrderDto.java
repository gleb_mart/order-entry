package com.netcracker.martynchyk.dto;

import com.fasterxml.jackson.annotation.*;
import com.netcracker.martynchyk.entity.OrderItem;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class OrderDto {

    private long id;
    private List<OrderItem> orderItems;
    @NotNull
    private Long customer;
    @NotNull
    @Min(1)
    private BigDecimal totalPrice;
    @NotNull
    @Min(0)
    private int totalAmount;
    @NotNull
    @Min(0)
    private double date;
    @NotNull
    private boolean paymentStatus;

    public OrderDto(long id, List<OrderItem> orderItems, Long customer, BigDecimal totalPrice,
                    int totalAmount, double date, boolean paymentStatus) {
        this.id = id;
        this.orderItems = orderItems;
        this.customer = customer;
        this.totalPrice = totalPrice;
        this.totalAmount = totalAmount;
        this.date = date;
        this.paymentStatus = paymentStatus;
    }

    public long getId() {
        return id;
    }

    @JsonIgnore
    public List<OrderItemDto> getOrderItemsDto() {
        return orderItems.stream()
                .map(orderItem -> orderItem.convertToDto())
                .collect(Collectors.toList());
    }

    public List<Long> getOrderItems() {
        return orderItems.stream()
                .map(orderItem -> orderItem.getOffer())
                .collect(Collectors.toList());
    }

    public Long getCustomer() {
        return customer;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public double getDate() {
        return date;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }
}
