package com.netcracker.martynchyk.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class OrderItemDto {
    @NotNull
    @Min(1)
    private Long offer;
    @NotNull
    @Min(1)
    private Long order;

    public OrderItemDto(Long offer, Long order) {
        this.offer = offer;
        this.order = order;
    }

    public Long getOffer() {
        return offer;
    }

    public Long getOrder() {
        return order;
    }

    public void setOffer(Long offer) {
        this.offer = offer;
    }

    public void setOrder(Long order) {
        this.order = order;
    }
}
