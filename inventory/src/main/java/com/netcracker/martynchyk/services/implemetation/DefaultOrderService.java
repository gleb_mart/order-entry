package com.netcracker.martynchyk.services.implemetation;

import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.dao.OrderDao;
import com.netcracker.martynchyk.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultOrderService implements OrderService {

    private final OrderDao orderDao;

    @Autowired
    public DefaultOrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public Order create(Order customer) {
        orderDao.create(customer);
        return customer;
    }

    @Override
    public Order find(long id) {
        return orderDao.find(id);
    }

    @Override
    public List<Order> findAll() {
        return orderDao.findAll();
    }

    @Override
    public Order update(Order customer) {
        orderDao.update(customer);
        return customer;
    }

    @Override
    public void delete(long id) {
        orderDao.delete(id);
    }
}
