package com.netcracker.martynchyk.services.implemetation;

import com.netcracker.martynchyk.entity.OrderItem;
import com.netcracker.martynchyk.dao.OrderItemDao;
import com.netcracker.martynchyk.services.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultOrderItemService implements OrderItemService {

    private final OrderItemDao orderItemDao;

    @Autowired
    public DefaultOrderItemService(OrderItemDao orderItemDao) {
        this.orderItemDao = orderItemDao;
    }

    @Override
    public OrderItem create(OrderItem orderItem) {
        orderItemDao.create(orderItem);
        return orderItem;
    }

    @Override
    public OrderItem find(long id) {
        return orderItemDao.find(id);
    }

    @Override
    public List<OrderItem> findAll() {
        return orderItemDao.findAll();
    }

    @Override
    public OrderItem update(OrderItem orderItem) {
        orderItemDao.update(orderItem);
        return orderItem;
    }

    @Override
    public void delete(long id) {
        orderItemDao.delete(id);
    }
}
