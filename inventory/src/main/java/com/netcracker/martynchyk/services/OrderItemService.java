package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.entity.OrderItem;

import java.util.List;

public interface OrderItemService {

    OrderItem create(OrderItem orderItem);

    List<OrderItem> findAll();

    OrderItem find(long id);

    OrderItem update(OrderItem orderItem);

    void delete(long id);
}
