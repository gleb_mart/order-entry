package com.netcracker.martynchyk.services;

import com.netcracker.martynchyk.entity.Order;

import java.util.List;

public interface OrderService {
    Order create(Order order);

    List<Order> findAll();

    Order find(long id);

    Order update(Order order);

    void delete(long id);
}
