package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.OrderItemDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;


@Getter
@Setter
@Entity
public class OrderItem extends BaseEntity {

    @Basic(optional = false)
    private Long offer;

    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    public OrderItem() {
    }

    public OrderItem(Long offer, Order order) {
        this.offer = offer;
        this.order = order;
    }

    public static OrderItem convertToEntity(OrderItemDto orderItemDto){
        OrderItem orderItem = new OrderItem();
        orderItem.setOffer(orderItemDto.getOffer());
        return orderItem;
    }

    public OrderItemDto convertToDto(){
        return new OrderItemDto(offer,order.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem1 = (OrderItem) o;
        return Objects.equals(offer, orderItem1.offer) &&
                Objects.equals(order, orderItem1.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(offer, order);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "offer=" + offer +
                '}';
    }
}