package com.netcracker.martynchyk.entity;

import com.netcracker.martynchyk.dto.OrderDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order extends BaseEntity {

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<OrderItem> orderItems = new ArrayList<>();

    @Basic(optional = false)
    private Long customer;

    @Basic(optional = false)
    private BigDecimal totalPrice;

    @Basic(optional = false)
    private int totalAmount;

    @Basic(optional = false)
    private double date;

    @Basic(optional = false)
    private boolean paymentStatus;


    public Order() {
    }

    public Order(List<OrderItem> orderItems, Long customer, BigDecimal totalPrice,
                 int totalAmount, double date, boolean paymentStatus) {
        this.orderItems = orderItems;
        this.customer = customer;
        this.totalPrice = totalPrice;
        this.totalAmount = totalAmount;
        this.date = date;
        this.paymentStatus = paymentStatus;
    }

    public static Order convertToEntity(OrderDto orderDto){
        Order order=new Order();
        order.setCustomer(orderDto.getCustomer());
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setTotalAmount(orderDto.getTotalAmount());
        order.setDate(orderDto.getDate());
        order.setPaymentStatus(orderDto.isPaymentStatus());
        return order;
    }

    public OrderDto convertToDto(){
        return new OrderDto(super.getId(),orderItems,customer,totalPrice,totalAmount,date,paymentStatus);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return totalAmount == order.totalAmount &&
                Double.compare(order.date, date) == 0 &&
                paymentStatus == order.paymentStatus &&
                Objects.equals(orderItems, order.orderItems) &&
                Objects.equals(customer, order.customer) &&
                Objects.equals(totalPrice, order.totalPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItems, customer, totalPrice, totalAmount, date, paymentStatus);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + super.getId() +
                "orderItems=" + orderItems +
                ", customer=" + customer +
                ", totalPrice=" + totalPrice +
                ", totalAmount=" + totalAmount +
                ", date=" + date +
                ", paymentStatus=" + paymentStatus + '\'' +
                '}';
    }
}