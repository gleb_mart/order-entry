package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.OrderItemDto;
import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.entity.OrderItem;
import com.netcracker.martynchyk.services.OrderItemService;
import com.netcracker.martynchyk.services.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/inventories/order-items")
public class OrderItemController {

    private final OrderItemService orderItemsService;

    @Autowired
    private OrderService orderService;

    @Autowired
    public OrderItemController(OrderItemService orderItemsService) {
        this.orderItemsService = orderItemsService;
    }

    @ApiOperation(value = "Return orderItem by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderItemDto get(
            @ApiParam(value = "Id of orderItem to lookup for", required = true)
            @PathVariable("id") Long id) {
        return orderItemsService.find(id).convertToDto();
    }

    @ApiOperation(value = "Return all orderItems")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<OrderItemDto> getAll() {
        List<OrderItem> orderItems = orderItemsService.findAll();
        return orderItems.stream()
                .map(orderItem -> orderItem.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create order item")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderItemDto createOrderItem(@RequestBody @Valid OrderItemDto orderItemDto) {
        OrderItem orderItem = OrderItem.convertToEntity(orderItemDto);
        Order findOrder = orderService.find(orderItemDto.getOrder());
        orderItem.setOrder(findOrder);
        OrderItem orderItemCreated = orderItemsService.create(orderItem);
        return orderItemCreated.convertToDto();
    }


    @ApiOperation(value = "Create many order items")
    @PostMapping(path = "/many")
    @ResponseStatus(HttpStatus.CREATED)
    public List<OrderItemDto> createManyOrderItem(@RequestBody @Valid List<OrderItemDto> orderItems) {
        OrderItem orderItem;
        OrderItem orderItemCreated;
        List<OrderItemDto> orderItemsCreated=new ArrayList<>();
        for(OrderItemDto orderItemDto:orderItems) {
            orderItem = OrderItem.convertToEntity(orderItemDto);
            orderItem.setOrder(orderService.find(orderItemDto.getOrder()));
            orderItemCreated = orderItemsService.create(orderItem);
            orderItemsCreated.add(orderItemCreated.convertToDto());
        }
        return orderItemsCreated;
    }



    @ApiOperation(value = "Update orderItem by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderItemDto updateOrderItem(
            @PathVariable("id") Long id,
            @RequestBody OrderItemDto orderItemDto) {
        OrderItem orderItem = orderItemsService.find(id);
        orderItem.setOffer(orderItemDto.getOffer());
        if (orderItemDto.getOrder() != orderItem.getOrder().getId()) {
            Order findCustomer = orderService.find(orderItemDto.getOrder());
            orderItem.setOrder(findCustomer);
        }
        return orderItemsService.update(orderItem).convertToDto();
    }

    @ApiOperation(value = "Delete orderItem by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderItem(@PathVariable("id") Long id) {
        orderItemsService.delete(id);
    }
}
