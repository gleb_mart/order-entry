package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.OrderDto;
import com.netcracker.martynchyk.dto.OrderItemDto;
import com.netcracker.martynchyk.entity.Order;
import com.netcracker.martynchyk.entity.OrderItem;
import com.netcracker.martynchyk.services.OrderItemService;
import com.netcracker.martynchyk.services.OrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "api/v1/inventories/orders")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "Return order by id")
    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto get(
            @ApiParam(value = "Id of order to lookup for", required = true)
            @PathVariable("id") Long id) {
        return orderService.find(id).convertToDto();
    }

    @ApiOperation(value = "Return all orders")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDto> getAll() {
        List<Order> orders = orderService.findAll();
        return orders.stream()
                .map(order -> order.convertToDto())
                .collect(Collectors.toList());
    }

    @ApiOperation(value = "Create order")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDto createOrderItem(@RequestBody @Valid OrderDto orderDto) {
        Order order = Order.convertToEntity(orderDto);
        Order orderCreated = orderService.create(order);
        return orderCreated.convertToDto();
    }

    @ApiOperation(value = "Update order by id")
    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto updateOrderItem(
            @PathVariable("id") Long id,
            @RequestBody @Valid OrderDto orderDto) {
        Order order = orderService.find(id);
        order.setPaymentStatus(orderDto.isPaymentStatus());
        order.setDate(orderDto.getDate());
        order.setTotalAmount(orderDto.getTotalAmount());
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setCustomer(orderDto.getCustomer());
        List<OrderItem> orderItems = new ArrayList<>();
        for (OrderItemDto orderItem : orderDto.getOrderItemsDto()) {
            orderItems.add(orderItemService.find(orderItem.getOffer()));
        }
        order.setOrderItems(orderItems);
        return orderService.update(order).convertToDto();
    }

    @ApiOperation(value = "Delete order by id")
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderItem(@PathVariable("id") Long id) {
        orderService.delete(id);
    }
}
