package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.implementation.DefaultOrderDao;
import com.netcracker.martynchyk.dao.OrderDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class DefaultOrderServiceConfig {

    @Bean
    public OrderDao orderDao() {
        return new DefaultOrderDao();
    }
}
