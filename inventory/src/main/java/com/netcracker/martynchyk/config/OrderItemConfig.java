package com.netcracker.martynchyk.config;


import com.netcracker.martynchyk.entity.OrderItem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class OrderItemConfig {

    @Bean
    public OrderItem orderItemA() {
        OrderItem orderItem = new OrderItem();
        orderItem.setOffer((long) 00);
        return orderItem;
    }

    @Bean
    public OrderItem orderItemB() {
        OrderItem orderItem = new OrderItem();
        orderItem.setOffer((long) 11);
        return orderItem;
    }

}
