package com.netcracker.martynchyk.config;

import com.netcracker.martynchyk.dao.OrderItemDao;
import com.netcracker.martynchyk.dao.implementation.DefaultOrderItemDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class DefaultOrderItemServiceConfig {

    @Bean
    public OrderItemDao orderItemDao() {
        return new DefaultOrderItemDao();
    }
}
