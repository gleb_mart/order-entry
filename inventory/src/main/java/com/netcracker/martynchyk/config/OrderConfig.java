package com.netcracker.martynchyk.config;


import com.netcracker.martynchyk.entity.Order;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ComponentScan(basePackages = "com.netcracker.martynchyk")
public class OrderConfig {

    @Bean
    public Order orderA() {
        Order order = new Order();
        order.setCustomer((long) 0);
        order.setTotalPrice(new BigDecimal(300));
        order.setTotalAmount(15);
        order.setDate(2000);
        order.setPaymentStatus(false);
        return order;
    }

    @Bean
    public Order orderB() {
        Order order = new Order();
        order.setCustomer((long) 1);
        order.setTotalPrice(new BigDecimal(100));
        order.setTotalAmount(1);
        order.setDate(1998);
        order.setPaymentStatus(true);
        return order;
    }
}
