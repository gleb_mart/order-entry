package com.netcracker.martynchyk.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class OrderItemDto {
    @NotNull
    private Long offer;
    @NotNull
    private Long order;

    public OrderItemDto() {
    }

    public OrderItemDto(Long offer, Long order) {
        this.offer = offer;
        this.order = order;
    }
}
