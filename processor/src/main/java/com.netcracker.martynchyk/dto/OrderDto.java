package com.netcracker.martynchyk.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Setter
public class OrderDto {
    private long id;
    @NotNull
    private List<Long> orderItems;
    @NotNull
    @Min(1)
    private Long customer;
    private BigDecimal totalPrice;
    private int totalAmount;
    private double date;
    private boolean paymentStatus;

    public OrderDto() {
    }

    public OrderDto(long id, List<Long> orderItems, Long customer, BigDecimal totalPrice,
                    int totalAmount, double date, boolean paymentStatus) {
        this.id = id;
        this.orderItems = orderItems;
        this.customer = customer;
        this.totalPrice = totalPrice;
        this.totalAmount = totalAmount;
        this.date = date;
        this.paymentStatus = paymentStatus;
    }


    public long getId() {
        return id;
    }

    public List<Long> getOrderItems() {
        return orderItems;
    }

    public Long getCustomer() {
        return customer;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    @JsonIgnore
    public double getDateEntity() {
        return date;
    }

    public Date getDate() {
        return new Date((long) date);
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }
}
