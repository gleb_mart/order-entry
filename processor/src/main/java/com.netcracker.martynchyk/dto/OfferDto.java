package com.netcracker.martynchyk.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
public class OfferDto {
    private String name;
    @NotNull
    private BigDecimal price;
    @NotNull
    @Min(1)
    private Long category;
    @NotNull
    private Set<TagDto> tags;
}
