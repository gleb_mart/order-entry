package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.exceptions.OrderAlreadyPayedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ProcessorExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    protected ResponseEntity<Object> handleNullPointerException(NullPointerException exc, WebRequest request) {
        String bodyOfResponce="There is no such content";
        return handleExceptionInternal(exc,bodyOfResponce,
                new HttpHeaders(), HttpStatus.NOT_FOUND,request);
    }

    @ExceptionHandler
    protected ResponseEntity<Object> handleHttpClientErrorException(HttpClientErrorException.BadRequest exc, WebRequest request) {
        String bodyOfResponce="External call bad request";
        return handleExceptionInternal(exc,bodyOfResponce,
                new HttpHeaders(), HttpStatus.NOT_FOUND,request);
    }

    @ExceptionHandler
    protected ResponseEntity<Object> handleHttpClientErrorException(HttpClientErrorException.NotFound exc, WebRequest request) {
        String bodyOfResponce="External call, not found";
        return handleExceptionInternal(exc,bodyOfResponce,
                new HttpHeaders(), HttpStatus.NOT_FOUND,request);
    }

    @ExceptionHandler
    protected ResponseEntity<Object> handleOrderAlreadyPayed(OrderAlreadyPayedException exc, WebRequest request) {
        String bodyOfResponce="Order already payed";
        return handleExceptionInternal(exc,bodyOfResponce,
                new HttpHeaders(), HttpStatus.NOT_FOUND,request);
    }

}
