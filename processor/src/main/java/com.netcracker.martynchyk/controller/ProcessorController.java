package com.netcracker.martynchyk.controller;

import com.netcracker.martynchyk.dto.OfferDto;
import com.netcracker.martynchyk.dto.OrderDto;
import com.netcracker.martynchyk.dto.OrderItemDto;
import com.netcracker.martynchyk.exceptions.OrderAlreadyPayedException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/processor")
public class ProcessorController {
    private final RestTemplate restTemplate;
    private final String catalogUrl;
    private final String inventoryUrl;

    @Autowired
    public ProcessorController(RestTemplate restTemplate,
                               @Value("${catalog.url}") String catalogUrl,
                               @Value("${inventory.url}") String inventoryUrl) {
        this.restTemplate = restTemplate;
        this.catalogUrl = catalogUrl;
        this.inventoryUrl = inventoryUrl;
    }


    @ApiOperation(value = "Create order")
    @PostMapping(path = "/orders")
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDto createOrder(@RequestBody @Valid OrderDto orderDto) {
        orderDto.setDate(new Date().getTime());

        UriComponentsBuilder catalogUriBuilder = UriComponentsBuilder.fromHttpUrl(catalogUrl + "/offers")
                .queryParam("ids", orderDto.getOrderItems().toArray());

        ResponseEntity<List<OfferDto>> response = restTemplate.exchange(
                catalogUriBuilder.toUriString(), HttpMethod.GET, null,
                new ParameterizedTypeReference<List<OfferDto>>() {
                });

        List<OfferDto> offers = response.getBody();
        BigDecimal totalPrice = new BigDecimal(0);

        for (OfferDto offer : offers) {
            totalPrice = totalPrice.add(offer.getPrice());
        }

        orderDto.setTotalPrice(totalPrice);
        orderDto.setTotalAmount(offers.size());
        orderDto.setPaymentStatus(false);

        List<Long> orderItems = orderDto.getOrderItems();
        orderDto.setOrderItems(null);

        HttpEntity<OrderDto> orderHttpEntity = new HttpEntity<>(orderDto);
        OrderDto savedOrderDto;

        savedOrderDto = restTemplate.exchange(inventoryUrl + "/orders", HttpMethod.POST,
                orderHttpEntity, OrderDto.class).getBody();
        Long orderId = savedOrderDto.getId();
        List<OrderItemDto> orderItemDtos = new ArrayList<>();

        for (long offer : orderItems) {
            orderItemDtos.add(new OrderItemDto(offer, orderId));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> orderItemHttpEntity = new HttpEntity<>(orderItemDtos, headers);

        List<OrderDto> savedOrderItems = restTemplate.exchange(inventoryUrl + "/order-items/many",
                HttpMethod.POST,
                orderItemHttpEntity,
                new ParameterizedTypeReference<List<OrderDto>>() {
                }).getBody();

        savedOrderDto.setOrderItems(orderItems);
        return savedOrderDto;
    }


    @ApiOperation(value = "Return the price of all orders")
    @GetMapping(path = "/{id}/calculate")
    @ResponseStatus(HttpStatus.OK)
    public BigDecimal calculateOrdersPrice(
            @ApiParam(value = "Id of customer for calculate total orders price", required = true)
            @PathVariable("id") Long customer) {
        ResponseEntity<List<OrderDto>> response = restTemplate.exchange(
                inventoryUrl + "/orders",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<OrderDto>>() {
                });
        List<OrderDto> orders = response.getBody();
        BigDecimal totalPrice = new BigDecimal(0);
        for (OrderDto order : orders) {
            if (order.getCustomer().equals(customer)) {
                totalPrice = totalPrice.add(order.getTotalPrice());
            }
        }
        return totalPrice;
    }

    @ApiOperation(value = "Count customer orders")
    @GetMapping(path = "/{id}/count")
    @ResponseStatus(HttpStatus.OK)
    public int countOrders(
            @ApiParam(value = "Id of customer to count orders", required = true)
            @PathVariable("id") Long customer) {
        ResponseEntity<List<OrderDto>> response = restTemplate.exchange(
                inventoryUrl + "/orders",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<OrderDto>>() {
                });

        List<OrderDto> orders = response.getBody();
        int numberOfOrders = 0;

        for (OrderDto order : orders) {
            if (order.getCustomer().equals(customer)) {
                numberOfOrders++;
            }
        }
        return numberOfOrders;
    }


    @ApiOperation(value = "Pay for the order")
    @GetMapping(path = "/orders/{id}/pay")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto payForOrder(
            @ApiParam(value = "Id of order for calculate total orders price", required = true)
            @PathVariable("id") Long orderId) {


        OrderDto payedOrder = restTemplate.exchange(inventoryUrl + "/orders/" + orderId.toString(),
                HttpMethod.GET,
                null,
                OrderDto.class).getBody();
        if (payedOrder.isPaymentStatus()) {
            throw new OrderAlreadyPayedException();
        }
        payedOrder.setPaymentStatus(true);
        payedOrder.setOrderItems(new ArrayList<>());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<OrderDto> orderHttpEntity = new HttpEntity<>(payedOrder, headers);

        String resourceUrl = inventoryUrl + "/orders/" + orderId.toString();
        ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(resourceUrl,
                HttpMethod.PUT,
                orderHttpEntity,
                OrderDto.class
        );

        return responseEntity.getBody();
    }


    @ApiOperation(value = "Get orders by payment status")
    @GetMapping(path = "/orders/{status}")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDto> getByPaymentStatus(
            @ApiParam(value = "Payment status of orders", required = true)
            @PathVariable("status") boolean reqiredPaymentStatus) {

        ResponseEntity<List<OrderDto>> response = restTemplate.exchange(
                inventoryUrl + "/orders",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<OrderDto>>() {
                });

        List<OrderDto> orders = response.getBody();
        List<OrderDto> ordersWithReqStatus = new ArrayList<>();
        for (OrderDto order : orders) {
            if (order.isPaymentStatus() == reqiredPaymentStatus) {
                ordersWithReqStatus.add(order);
            }
        }
        return ordersWithReqStatus;
    }



    @ApiOperation(value = "Add order item")
    @PostMapping(path = "/orders/order-items")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto addOrderItem(@RequestBody @Valid OrderItemDto orderItemDto) {

        OrderDto order = restTemplate.exchange(inventoryUrl + "/orders/" + orderItemDto.getOrder().toString(),
                HttpMethod.GET,
                null,
                OrderDto.class).getBody();

        List<Long> newOrderItems=order.getOrderItems();
        newOrderItems.add(orderItemDto.getOffer());
        order.setOrderItems(newOrderItems);

        UriComponentsBuilder catalogUriBuilder = UriComponentsBuilder.fromHttpUrl(catalogUrl + "/offers")
                .queryParam("ids", order.getOrderItems().toArray());

        ResponseEntity<List<OfferDto>> response = restTemplate.exchange(
                catalogUriBuilder.toUriString(), HttpMethod.GET, null,
                new ParameterizedTypeReference<List<OfferDto>>() {
                });

        List<OfferDto> offers = response.getBody();
        BigDecimal totalPrice = new BigDecimal(0);

        for (OfferDto offer : offers) {
            totalPrice = totalPrice.add(offer.getPrice());
        }

        order.setTotalPrice(totalPrice);
        order.setTotalAmount(offers.size());
        order.setPaymentStatus(false);

        List<Long> orderItems = order.getOrderItems();
        order.setOrderItems(null);

        HttpEntity<OrderDto> orderHttpEntity = new HttpEntity<>(order);
        OrderDto savedOrderDto;

        String resourceUrl = inventoryUrl + "/orders/" + order.getId();

        savedOrderDto = restTemplate.exchange(resourceUrl, HttpMethod.PUT,
                orderHttpEntity, OrderDto.class).getBody();

        Long orderId = savedOrderDto.getId();

        List<OrderItemDto> orderItemDtos = new ArrayList<>();

        for (long offer : orderItems) {
            orderItemDtos.add(new OrderItemDto(offer, orderId));
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> orderItemHttpEntity = new HttpEntity<>(orderItemDtos, headers);

        List<OrderDto> savedOrderItems = restTemplate.exchange(inventoryUrl + "/order-items/many",
                HttpMethod.POST,
                orderItemHttpEntity,
                new ParameterizedTypeReference<List<OrderDto>>() {
                }).getBody();

        savedOrderDto.setOrderItems(orderItems);

        return savedOrderDto;
    }




}
